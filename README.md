# nvim-restremind

**You need to build better working habits!**

nvim-restremind is a plugin for Neovim that allows you, the user, to create better working habits through reminders for breaks and working sessions.

## Features
- Automatic reminders for breaks and working sessions
- Customisation for break and working durations
- Notification through a pop-up floating window with customisable ASCII banner
- Blazingly 🔥 fast 🚀✨
    - Written in Rust using nvim-oxi

## Installation
**Only for Linux at this moment, if you want to install it on other OS, please compile it yourself!**

Using lazy.nvim:
```
{
    "https://gitlab.com/david-0609/nvim-restremind",
    event = "BufRead",
    build = "install.sh"
    config = function()
        require("nvim-restremind").start_session()
    end
}
```

Using packer.nvim

```
{
    "https://gitlab.com/david-0609/nvim-restremind",
    event = "BufRead",
    run = "install.sh"
    config = function()
        require("nvim-restremind").start_session()
    end
}
```

Set the event to `BufRead` so that it loads on opening your first buffer, or you can use the `ft` field to define file types that trigger the start of the timer, but `BufRead` is recommended as it loads on the first buffer you open that is not `alpha.nvim`.

## Configuration

**This plugin does not come with default options, you must configure it yourself!**

The `start_session()` function takes in a Lua table. See example:
```
{
    message = "Take a break",
    work_duration = 40,
    rest_duration = 5,
    enabled = true
    initial_state = "working"
}
```
The work and rest duration fields take in an integer that represents the number of minutes per session. The message will be processed into ASCII art using figlet-rs, so it should be short in order to fit on your screen. The initial_state field is the state the plugin starts in, if it is set to working, the work session will start when you open a file. 

